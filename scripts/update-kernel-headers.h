#!/bin/sh

kernel_version="5.15"
tmpdir="$(mktemp -d)"
topdir="$(pwd)"
kernelhdrdir="$(realpath $topdir/../kernel_headers)"
arch="$1"

case "$arch" in
	x86) karch="i386"; target="i386-linux-ouma" ;;
	x86_64) karch="x86_64"; target="x86_64-linux-ouma" ;;
	*) exit 1 ;;
esac

cd "$tmpdir"
curl -C - -L -O https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$kernel_version.tar.xz
bsdtar -xf linux-$kernel_version.tar.xz
cd linux-$kernel_version
make mrproper
make ARCH=$karch headers
find usr/include -name '.*' -exec rm -rf {} +
rm usr/include/Makefile
cp -r usr/include/* "$kernelhdrdir"/
cd "$kernelhdrdir"
mkdir $target
mv asm $target/asm
cd "$topdir"
rm -rf "$tmpdir"
