#include <stdint.h>

#ifndef RSIZE_MAX
#define RSIZE_MAX (SIZE_MAX >> 1)
#endif

void __throw_constraint_handler_s(const char * restrict msg, int error);
