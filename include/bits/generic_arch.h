#ifndef _BITS_GENERIC_ARCH_H_
#define _BITS_GENERIC_ARCH_H_

#define	__S16_TYPE		short int
#define __U16_TYPE		unsigned short int
#define	__S32_TYPE		int
#define __U32_TYPE		unsigned int
#define __SLONGWORD_TYPE	long int
#define __ULONGWORD_TYPE	unsigned long int
#if __LP64__
#define __SQUAD_TYPE		long int
#define __UQUAD_TYPE		unsigned long int
#define __SWORD_TYPE		long int
#define __UWORD_TYPE		unsigned long int
#define __SLONG32_TYPE		int
#define __ULONG32_TYPE		unsigned int
#define __S64_TYPE		long int
#define __U64_TYPE		unsigned long int
#define __STD_TYPE		typedef
#else
#define __SQUAD_TYPE		__int64_t
#define __UQUAD_TYPE		__uint64_t
#define __SWORD_TYPE		int
#define __UWORD_TYPE		unsigned int
#define __SLONG32_TYPE		long int
#define __ULONG32_TYPE		unsigned long int
#define __S64_TYPE		__int64_t
#define __U64_TYPE		__uint64_t
#define __STD_TYPE		__extension__ typedef
#endif

#endif /* !_BITS_GENERIC_ARCH_H_ */
