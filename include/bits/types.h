/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2002 Mike Barcroft <mike@FreeBSD.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _BITS_TYPES_H_
#define _BITS_TYPES_H_

#include <sys/cdefs.h>
#include <bits/core_types.h>

#ifdef __x86_64__
#include <bits/x86_64.h>
#elif __i386__
#include <bits/x86.h>
#else
#error Unsupported platform
#endif

/*
 * Standard type definitions.
 */
typedef __UQUAD_TYPE		__dev_t;	/* Type of device numbers.  */
typedef __U32_TYPE		__uid_t;	/* Type of user identifications.  */
typedef __U32_TYPE		__gid_t;	/* Type of group identifications.  */
typedef __ULONGWORD_TYPE	__ino_t;	/* Type of file serial numbers.  */
typedef __UQUAD_TYPE		__ino64_t;	/* Type of file serial numbers (LFS).*/
typedef __U32_TYPE		__mode_t;	/* Type of file attribute bitmasks.  */
typedef __UWORD_TYPE		__nlink_t;	/* Type of file link counts.  */
typedef __SLONGWORD_TYPE	__off_t;	/* Type of file sizes and offsets.  */
typedef __SQUAD_TYPE		__off64_t;	/* Type of file sizes and offsets (LFS).  */
typedef __S32_TYPE		__pid_t;	/* Type of process identifications.  */
typedef struct { int val[2]; }	__fsid_t;	/* Type of file system IDs.  */
typedef __SLONGWORD_TYPE	__clock_t;	/* Type of CPU usage counts.  */
typedef __ULONGWORD_TYPE	__rlim_t;	/* Type for resource measurement.  */
typedef __UQUAD_TYPE		__rlim64_t;	/* Type for resource measurement (LFS).  */
typedef __U32_TYPE		__id_t;		/* General type for IDs.  */
typedef __SLONGWORD_TYPE	__time_t;	/* Seconds since the Epoch.  */
typedef __U32_TYPE		__useconds_t;   /* Count of microseconds.  */
typedef __SLONGWORD_TYPE	__suseconds_t;  /* Signed count of microseconds.  */
typedef __SQUAD_TYPE		__suseconds64_t;
typedef __S32_TYPE		__daddr_t;	/* The type of a disk address.  */
typedef __S32_TYPE		__key_t;	/* Type of an IPC key.  */
typedef __S32_TYPE		__clockid_t;
typedef void			__timer_t;
typedef __SLONGWORD_TYPE	__blksize_t;
typedef __SLONGWORD_TYPE	__blkcnt_t;
typedef __SQUAD_TYPE		__blkcnt64_t;
typedef __ULONGWORD_TYPE	__fsblkcnt_t;
typedef __UQUAD_TYPE		__fsblkcnt64_t;
typedef __ULONGWORD_TYPE	__fsfilcnt_t;
typedef __UQUAD_TYPE		__fsfilcnt64_t;
typedef __SWORD_TYPE		__fsword_t;
typedef __SWORD_TYPE		__ssize_t;
typedef __SYSCALL_SLONG_TYPE	__syscall_slong_t;
typedef __SYSCALL_ULONG_TYPE	__syscall_ulong_t;
typedef __off64_t		__loff_t;
typedef char			*__caddr_t;
typedef __SWORD_TYPE		__intptr_t;
typedef __U32_TYPE		__socklen_t;
typedef int			__sig_atomic_t;

/* Clang already provides these types as built-ins, but only in C++ mode. */
#if !defined(__clang__) || !defined(__cplusplus)
typedef	__uint_least16_t __char16_t;
typedef	__uint_least32_t __char32_t;
#endif
/* In C++11, char16_t and char32_t are built-in types. */
#if defined(__cplusplus) && __cplusplus >= 201103L
#define	_CHAR16_T_DECLARED
#define	_CHAR32_T_DECLARED
#endif

#ifdef __GNUCLIKE_BUILTIN_VARARGS
typedef	__builtin_va_list	__va_list;	/* internally known to gcc */
#else
#error "No support for your compiler for stdargs"
#endif
#if defined(__GNUC_VA_LIST_COMPATIBILITY) && !defined(__GNUC_VA_LIST) \
    && !defined(__NO_GNUC_VA_LIST)
#define __GNUC_VA_LIST
typedef __va_list		__gnuc_va_list;	/* compatibility w/GNU headers*/
#endif

#endif /* !_BITS_TYPES_H_ */
