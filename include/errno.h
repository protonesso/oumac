#ifndef _ERRNO_H_
#define _ERRNO_H_

#include <linux/errno.h>

#ifndef __error_t_defined
typedef int error_t;
#define __error_t_defined
#endif
#endif /* _ERRNO_H_ */
